# CSS styleguide
This project is meant to be a guideline for how developers should write their SCSS/CSS within TouchTribe. 

**DISCLAIMER:** This document is heavily inspired by the AirBnB css style guide and is my interpretation of it. 
In the end this document should be a living document where everyone participates in maintaining it.   

## Motivation
  Until this moment we (as in TouchTribe) didn't have a style guide for (S)CSS. The purpose of having a style guide is 
 to start writing our styles in a more uniform way, which will make both reviewing code 
 and joining new project a lot easier.  

## Table of Contents

1. [Methodologies and Basic Rules](#methodologies-and-basic-rules)
1. [Sass](#sass)
1. [File Structure](#file-structure)
1. [Stylelint](#stylelint)
1. [Contributing](#contributing)

## Methodologies and Basic Rules
Within TouchTribe we use [BEM](http://getbem.com/) when writing css but we extend it with a prefix to give more context 
to a classname, this improves readability for other developers and helps separating concerns when writing the code.

### Basic Rules
Here are some standard rules when writing css.
* Use soft tabs (2 spaces) for indentation.
* Use dashes class names (kebab-case).
* Do not use ID selectors.
* When using multiple selectors in a rule declaration, give each selector its own line.
* Put a space before the opening brace `{` in rule declarations.
* In properties, put a space after, but not before, the `:` character.
* Put closing braces `}` of rule declarations on a new line.
* Put blank lines between rule declarations.

**Bad**

```css
.avatar{
    border-radius:50%;
    border:2px solid white; }
.no, .nope, .not_good {
    // ...
}
#lol-no {
  // ...
}
```

**Good**

```css
.avatar {
  border-radius: 50%;
  border: 2px solid white;
}

.one,
.selector,
.per-line {
  // ...
}
```

### Comments
* Write detailed comments for code that isn't self-documenting:
  - Uses of z-index
  - Compatibility or browser-specific hacks

### Class Prefixes
Here's a list of the prefixes that can be used, each prefix has a short explanation with an example.
* `.l-`: layout classes
* `.c-`: component classes
* `.u-`: utility classes
* `.is-`|`.has-`: state classes
* `.js-`: JavaScript classes

#### Layout Classes 
A layout class is responsible for the layout of pages, components, etc. I should hold no styling e.g. `background-color: red;`.
```scss
.l-container {
  width: 100%;
  max-width: 1140px;
  padding: 0 32px;
  margin: 0 auto;
}
```

#### Component Classes
A component is a UI element, it can contain other components and should as content agnostic as possible. 
Which means that a `.c-card` should look good no matter if it is put in a main container or a sidebar.
```scss
.c-card {
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 16px;
  box-shadow: 1px 1px 4px 0 rgba(#000, .25);
}
``` 

#### Utility Classes
A utility class is a class that can be used on an HTML element that doesn't have any specific classes but still needs some styling.
```scss
.u-text-center {
  text-align: center;
}
```

#### State Classes
State classes say something about the state of a component. For example when the component `is` selected. 
This is something different than a modifier class used by BEM. This is a state that for example is set by JavaScript.
```scss
.c-card.is-selected {
  border-color: limegreen;
}
```

#### JavaScript Classes
JavaScript classes should never hold any styling but should purely be used to select an element using JavaScript.
```javascript
const card = document.querySelector('.js-card')
```

To learn more about BEM read the following articles:
* [Smashing Magazine BEM 101](https://css-tricks.com/bem-101/)
* [CSS Wizardry MindBEMding](https://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)

## Sass
Sass is our preferred preprocessor for css. Here are some basic rules for sass specifically.
* Use `.scss` as a file extension, never the original `.sass` file extension.
* Start the filename of a partial with an underscore, so Sass will actually recognize it as a partial: `_partial.scss`.
* Write your variables in kebab-case: `$my-variable`.
* Avoid the use of the `@extend` directive since it can cause unexpected behaviour.
* Inception rule: Try to minimize nesting and never nest more than 3 levels deep. 
---
> *Never nest more than 3 levels deep.*
![Inception](https://data.junkee.com/wp-content/uploads/2018/08/Inception.png)
---
Example:
```scss
/* Wrong */
.c-main-navigation {
  width: 100%;

  ul {
    background-color: blue;

    li {
      display: inline-block;
      margin: 0 5px;

      a {
        text-decoration: none;
      }
    }
  }
}

/* Right */
.c-main-navigation {
  width: 100%;
}

.c-main-navigation__menu {
  background-color: blue;
}


.c-main-navigation__item {
  display: inline-block;
  margin: 0 5px;
}

.c-main-navigation__link {
  text-decoration: none;
}
```

### Anatomy of a Style Rule
To keep style rules structured and easily maintainable it's a good idea to stick to an specific order.
```scss
.c-my-component {
  // List all standard property declarations first
  padding: 0 10px;
  background-color: aquamarine;
  // Group @include declarations below
  @include transition(opacity, .5s, ease-in-out);
  
  // Add pseudo selectors & elements below, add whitespace in between
  &:hover {
    background-color: aliceblue;
  }

  &:after {
    content: "I'm pseudo as a mofo";
  }

  // Add nested selectors below, add whitespace in between
  .c-my-component__element {
    color: mediumpurple;
  }
}
``` 

## File Structure
Sometimes it can be quite project specific how folders and files are structured but most existing projects follow quite a similar pattern.
The most important thing here is that the style rules start generic and increasingly become more specific. 
A file that is super generic like `_button.scss` probably contains a lot of code, 
while a more specific file like `_home-page.scss` should probably contain less code since the components used on that page 
are already styled via their own classes. 
```
./SCSS
|./base // Contains variables, mixins, etc. but no actual style rules
|   |-_colors.scss
|   |-_fonts.scss
|   |-_mixins.scss
|./basics // Contains basic styling and styling of the smallest elements 
|   |-_reset.scss
|   |-_typography.scss
|   |-_layout.scss
|   |-_button.scss
|./components // Contains styling for components and UI elements 
|   |-_header.scss
|   |-_card.scss
|   |-_menu.scss
|./specifics // Contains styling specific for a certain page or section 
|   |-_page-home.scss
|   |-_page-detail.scss
|   |-_section-contact.scss
_shame.scss // For quick fixes, has to be refactored later
style.scss // Imports all the partials
``` 

## Stylelint
To enforce some of the rules while coding, make use of the `.stylelintrc` included in this repository. 
To include it in existing projects copy the file and install the dependencies via npm.
```
npm install stylelint stylelint-config-standard --save-dev
```
After installing add the stylelint script (`stylelint --fix`) to your `lint-staged` configuration targeting the correct files.  

**IMPORTANT NOTE:** Since there's a lot of projects where you will have to overwrite some styles of for example a Magento theme,
you sometimes can't evade the use of `!important` or super specific selector combinations. 
To learn how to disable the stylelint for specific lines or files please read the [documentation](https://stylelint.io/user-guide/configuration#turning-rules-off-from-within-your-css).   

## Contributing
To contribute to this file please open an issue or create a merge request with your proposed changes.
